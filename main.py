import pandas as pd
import numpy as np
import csv

dir_Teachers = './data/teachers.csv'
dir_Students = './data/studentNames.csv'
dir_Courses = './data/courses.csv'
dir_StudentCourse = './data/studentCourse.csv'

with open(dir_Teachers,'r', encoding='utf-8-sig') as t:
    reader = csv.reader(t)
    teacher= list(reader)
t.close()
teacher = list(filter(None, teacher))

with open(dir_Students, mode='r', encoding='utf-8-sig') as sn:
    reader = csv.reader(sn)
    studentName = list(reader)
sn.close()

with open(dir_StudentCourse, mode='r', encoding='utf-8-sig') as sc:
    reader = csv.reader(sc)
    studentCourse = list(reader)
sc.close()
del studentCourse[:1]

with open(dir_Courses, mode='r', encoding='utf-8-sig') as c:
    reader = csv.reader(c)
    course = list(reader)
c.close()

#print(teacher)
#print(studentName)
#print(course)
#print(studentCourse)


# Xóa biểu mẫu trùng lặp trong Danh sách khóa học
# cc,cn = zip(*course)
find_dup = {}
found_dup = []
reported_single = []
#print("Original List = ",len(course))
for index in course:
    find_dup[index[0]] = find_dup.get(index[0], 0) + 1
    if (find_dup[index[0]] > 1):
      found_dup.append(index)
# print(find_dup)
# print("Duplicates  = ",found_dup)
for index in course:
    if find_dup[index[0]] == 1:
        reported_single.append(index)
# reported_single
reported_single.extend(found_dup)
#print("New List = ",len(reported_single))
course = reported_single
# print(len(course))
# print(cc)

# hàm tìm tên khóa học theo mã khóa hoc
def findNameCourseByName(course, keyCourse):
  for cod, name in course:
    if keyCourse == cod:
      return name
  return ''

# Điều phối sinh viên với các khóa học tương ứng của họ
# Trả về một nhóm sinh viên và tất cả các khóa học tương ứng của họ
def similar(person):
  dup = set()
  for row in studentCourse:
    # print("Row: ",row[0])
    if row[1] == person:
      dup.add(row[2])
      #print("find ",row[1])
  return dup
  # print(dup)

# print(len(studentCourse))
# course_of = similar("1759001-Phạm Thiên An")
# print("course_of " , course_of)


all_course = dict()
for p1 in studentName:
  person = p1[0]
  course1 = similar(person)
  all_course[person] = list(course1)
  
#print(all_course)

# Trả về một tập hợp các khóa học và tất cả các sinh viên tương ứng của họ
def take_course(subject):
  dup = set()
  for row in studentCourse:
    # print("Row: ",row[1])
    if row[2] == subject:
      dup.add(row[1])
  #    print(row[1],":",des)
  return dup
  # print(dup)
all_person = dict()
all_find = dict()
cc,cn = zip(*course)
size_pe = 0
# print(len(studentCourse))
for subject in cc:
  # print(person)
  person1 = take_course(subject)
  people = list(person1)
  size_pe= size_pe + len(people)
  all_person[subject] = people
  all_find[subject] = len(people)
#size_pe
#print(all_person)
#all_find

more_rooms = {}
for course_num in all_find:
  if all_find[course_num] >30:
    more_rooms[course_num] = True
  else:
    more_rooms[course_num] = False
#print(more_rooms)

#print(all_find)

# Tạo 10 phòng thi
Rooms = ['1.1', '1.2','1.3', '2.1','2.2', '2.3','3.1', '3.2','3.3', '3.4']
# Mỗi buổi thi được tổ chức từ 8 giờ sáng đến 4 giờ chiều
# và chia ra 2 buổi mỗi buổi 3 tiếng
Timeslot = ['8am-11am','1pm-4pm']
# Kỳ thi sẽ được tổ chức vào các ngày trong tuần, trừ thứ 7 và chủ nhật
Day = ["2_Monday", "3_Tuesday", "4_Wednesday", "5_Thursday", "6_Friday"]

# Mỗi khóa học được lên lịch thi trong kỳ thi 1 cách ngẫu nhiên
# Tạo một gen duy nhất / Timeslot
# [0, '2_Monday', '1pm-4pm', 'Nguyễn Lâm Sanh', '2.2']
def make_slots(course):
  week1 = [np.random.randint(0, high=4)]  # Week
  day1 = [Day[np.random.randint(0, high=len(Day))]] #Day
  timeslot1 = [Timeslot[np.random.randint(0, high=len(Timeslot))]]
  teach1 = teacher[np.random.randint(0, high=len(teacher))] #teacher
  room1 = [Rooms[np.random.randint(0, high=len(Rooms))]] #Room 
  return (week1+day1+timeslot1+teach1+room1)

#print(make_slots(course[0]))


# Tạo ra nhiều nhiễm sắc thể, mỗi nhiễm sắc thể có nhiều gen bên trong 
# mỗi nhiễm sắc thể có cùng cấu trúc gen (Timeslot)
def timeslot_chromosome():
  course_gen = dict()
  course_code, course_name = zip(*course)
  student_selection = []
  # schedule_students = []
  # print(course_code[1])
  gene_list = 0

  # Kích thước nhiễm sắc thể phụ thuộc vào số khóa học
  for c1 in course_code:
    students_sort = []
    # Tìm Khóa học có hơn 30 sinh viên để có thể tách ra thêm một khóa học riêng biệt 
    # vì mỗi phòng thi chỉ có tối đa 30 người thi
    if more_rooms[c1]==True:
      students_sort = all_person[c1]
      # np.random.shuffle(students_sort)
      students_split = [students_sort[i:i+30] for i in range(0, len(students_sort), 30)]
      # print(c1,": ",len(students_sort) , ": ",students_sort)
      # print("---> ",c1,": ",len(students_split[0]) ,": ",len(students_split[1]) ,": ",students_split)
      # print(c1)
      # print(len(students_split))
      # break

      # Đây là vị trí tạo ra gens [tuần, ngày, buổi, giáo viên, phòng]
      # và tạo ra 1 cách ngẫu nhiên
      # sau đó gán vào nhiễm sắc thể 
      # với cấu trúc: MãKhóaHọc, [tuần, ngày, buổi, giáo viên, phòng], Students_Of_Course
      main_slot=make_slots(c1)
      schedule_students = [c1,main_slot,students_split[0]]
      course_gen[gene_list] = schedule_students
      gene_list = gene_list + 1

      for i in range(1,len(students_split)):
        # This is the slot that generates room, invigilator(week,day,slot is same)        
        # Đối với khóa học có hơn 30 người thi sẽ tao ra 1 nhiễm sắc thể mới
        # có cùng gen MãKhóaHọc, [tuần, ngày, buổi, ..], Students_Of_Course_Splited
        # gen giáo viên và phòng học sẽ được thay đổi ngẫu nhiên
        slot1=make_slots(c1)
        
        slot1[0] = main_slot[0]
        slot1[1] = main_slot[1]
        slot1[2] = main_slot[2]

        schedule_students = [c1,slot1,students_split[i]]
        course_gen[gene_list] = schedule_students
        gene_list = gene_list + 1
        # print(c1,': ',students_split[i])
      
    else:
      slot1=make_slots(c1)
      schedule_students = [c1,slot1,students_sort] 
      course_gen[gene_list] = schedule_students
      gene_list = gene_list + 1
      
  # print(c1,course_gen[c1])
  # print(course_gen)
  # print(course_code)
  # calculate_fitness(course_gen)
  return course_gen

# tính toán thể trạng (Điểm thể dục) của các nhiễm sắc thể đã cho
# chromosome là 1 dictionary chứa tất cả các nhiễm sắc thể của 1 cá thể
# cá thể ở đây là 1 lịch thi cho 1 kỳ thi (timetable)
def calculate_fitness(chromosome : dict) -> int :
    # chromosome là [
    # 0: ['CS217', [0, '2_Monday', '1pm-4pm', 'Phan Chí Chính', '2.2'], [Students_Of_Course],
    # 1: ['CS217', [0, '2_Monday', '1pm-4pm', 'Trần Tuấn Kiệt', '1.2'], [Students_Of_Course],
    # ...
    # ]
    
    # Tổng số điểm
    overall_score = 0
    # giáo viên có hai bài kiểm tra đồng thời
    Teacher_Two_Same_Time_Exams = True
    # học sinh có hai bài kiểm tra đồng thời
    Student_Two_Same_Time_Exams = True
    # Phòng thi có 2 bài kiểm tra đồng thời
    Classroom_Two_Same_Time_Exams = True
    # Không có giáo viên nào coi thi liên tiếp nhau trong 1 ngày
    Invigilator_Consecutive_Exams =  True
    # Mỗi phòng thi có tối đa 30 người
    Max_30_Student = True
    
    for i in chromosome:
      for j in chromosome:
        if j>i:
          #  Kiểm tra để đảm bảo không có giáo viên nào có hai buổi thi liên tiếp
          if (chromosome[i][1][0]==chromosome[j][1][0] and chromosome[i][1][1]==chromosome[j][1][1] and chromosome[i][1][3]==chromosome[j][1][3]):
            if chromosome[i][1][2]!=chromosome[j][1][2]:
              Invigilator_Consecutive_Exams =False


          if (chromosome[i][1][0]==chromosome[j][1][0] and chromosome[i][1][1]==chromosome[j][1][1] and chromosome[i][1][2]==chromosome[j][1][2]):
            # Kiểm tra xem một giáo viên có coi thi hai bài kiểm tra ở cùng một thời điểm không 
            if chromosome[i][1][3]==chromosome[j][1][3]:
              # print("SAME EXAM TEACHER\t",chromosome[i][1][3],"Duplicate Teacher in Course ",chromosome[i][0],"And ",chromosome[j][0])
              Teacher_Two_Same_Time_Exams=False
            # Kiểm tra xem một học sinh có hai bài kiểm tra ở cùng một thời điểm không
            for k in range(0,len(chromosome[i][2])):
              for l in range(0,len(chromosome[j][2])):
                # print ("K ",k," And ",l)
                if(chromosome[i][2][k]==chromosome[j][2][l]):
                  # print("SAME EXAM TIME(STUDENTS)\t",chromosome[i][2][k],"Duplicate Student in Course ",chromosome[i][0],"And ",chromosome[j][0])
                  # print(chromosome[i][2][k],"Duplicate Student in Course ","       ",chromosome[i][0],"And ",chromosome[j][0],"  ",chromosome[i][1],"  ",chromosome[j][1])
                  Student_Two_Same_Time_Exams = False
                  # break
            # Kiểm tra xem một phòng học có tổ chức hai bài thi cùng một thời điểm không
            if chromosome[i][1][4]==chromosome[j][1][4]:
              # print("SAME EXAM CLASSROOM\t",chromosome[i][1][4],"Duplicate Room in Course ",chromosome[i][0],"And ",chromosome[j][0])
              Classroom_Two_Same_Time_Exams=False

    if (Teacher_Two_Same_Time_Exams):
        overall_score = overall_score + 1

    if (Student_Two_Same_Time_Exams):
        overall_score = overall_score + 1

    if (Classroom_Two_Same_Time_Exams):
        overall_score = overall_score + 1

    if (Invigilator_Consecutive_Exams):
        overall_score = overall_score + 1

    if (Max_30_Student):
        overall_score = overall_score + 1
        
    # Nếu điểm tổng overall_score = 5 
    # là đảm bảo được tất cả điều kiện của bài toán
    return overall_score

# Khởi tạo mảng quần thể, thêm nhiều cá thể vào mảng
# Làm đầy quần thể bằng cách tạo ngẫu nhiên các cá thể với các nhiễm sắc thể được sinh ngẫu nhiên (Timetables)
Populations_array = []
def initialize_population():
  # tạo quần thể với số lượng cá thể được chỉ định
  Total_population = 700
  for i in range(Total_population):
      chromosome = timeslot_chromosome()
      Populations_array.append(chromosome)

# Chọn hai cá thể tốt nhất từ ​​mảng quần thể
# với cơ chế bánh xe chọn lọc cá thể dựa trên điểm thể lực
# với điểm thể lực càng cao thì có xác suất chọn càng cao
def roulette_wheel_selection(population_array):
    fitness_array = []
    for i in range(len(population_array)):
        fitness_array.append(calculate_fitness(population_array[i]))
    #print(fitness_array)
    total_sum = sum(fitness_array)

    probability_array = []

    for i in range(len(fitness_array)):
        probability_array.append(fitness_array[i] / total_sum)
    #print(probability_array)

    index_population_array = []
    for i in range(len(population_array)):
        index_population_array.append(i)
    #print(index_population_array)

    returned_index_array = np.random.choice(index_population_array, 2, p=probability_array)

    return returned_index_array

# Tạo ra 2 cá thể con cái từ hai cá thể cha mẹ
# sau khi cá thể cha mẹ được chọn lọc tự nhiên từ quần thể
def crossover(cromosome_1: list, cromosome_2: list):
    # Tạo ngẫu nhiên lát cắt nhiễm sắc thể trong cá thể cha mẹ
    slice_no = np.random.randint(0, high=len(cromosome_1))
    # print(slice_no)
    
    # Để chia Cá thể một thành hai phần
    upper_slice_one = []
    lower_slice_two = []

    # Để chia Cá thể hai thành hai phần
    upper_slice_two = []
    lower_slice_one = []

    for i in range(0,slice_no):
        upper_slice_one.append(cromosome_1[i])
        lower_slice_two.append(cromosome_2[i])
    for j in range(slice_no, len(cromosome_1)):
        upper_slice_two.append(cromosome_1[j])
        lower_slice_one.append(cromosome_2[j])

    # print(upper_slice_one)
    # print(lower_slice_two)
 
    # print(upper_slice_two)
    # print(lower_slice_one)
 
    merged_chromosome_one = dict()
    merged_chromosome_two = dict()
    dup_1 = 0
    dup_2 = 0
    for i in range(len(upper_slice_one)):
      merged_chromosome_one[dup_1] = upper_slice_one[i]
      dup_1 = dup_1 + 1
    for i in range(len(lower_slice_one)):
      merged_chromosome_one[dup_1] = lower_slice_one[i]
      dup_1 = dup_1 + 1
    for i in range(len(upper_slice_two)):
      merged_chromosome_two[dup_2] = upper_slice_two[i]
      dup_2 = dup_2 + 1
    for i in range(len(lower_slice_two)):
      merged_chromosome_two[dup_2] = lower_slice_two[i]
      dup_2 = dup_2 + 1

    return merged_chromosome_one, merged_chromosome_two

# Thay thế nhiễm sắc thể cũ bằng nhiễm sắc thể mới nếu thể trạng của nhiễm sắc thể mới 
# lớn hơn mức tối thiểu của nhiễm sắc thể cũ
def exchange_lowest_fitness(chromosome_1 : dict, chromosome_2 : dict):
  fitness_array = []
  for i in range(len(Populations_array)):
    fitness_array.append(calculate_fitness(Populations_array[i]))

  lowest = fitness_array[0]
  # print(lowest)
  second_lowest = 0
  for i in fitness_array[1:]:
    if i < lowest:
      second_lowest = lowest
      lowest = i
    elif second_lowest == None or second_lowest > i:
      second_lowest = i
      
  
  lowest_index = fitness_array[lowest]
  second_lowest_index = fitness_array[second_lowest]

  if (calculate_fitness(chromosome_1) >= lowest):
    Populations_array.pop(lowest_index)
    Populations_array.append(chromosome_1)
    mutation_(Populations_array[9])

  lowest = fitness_array[0]
  # print(lowest)
  for i in fitness_array[1:]:
    if i < lowest:
      second_lowest = lowest
      lowest = i
    elif second_lowest == None or second_lowest > i:
      second_lowest = i

  if second_lowest == 0:
      second_lowest = lowest
  second_lowest_index = fitness_array.index(second_lowest)

  if (calculate_fitness(chromosome_2) >= second_lowest):
      Populations_array.pop(second_lowest_index)
      Populations_array.append(chromosome_2)
      mutation_(Populations_array[9])

      # print("after mutation", Populations_array[lowest_index])

# Thực hiện đột biến trên một nhiễm sắc thể nhất định với 
# tỉ lệ đột biến chọn lọc (tỷ lệ 3%)
MutRate = 3
def mutation_(chromosome):
  rand_mutation_rate = np.random.randint(0, high=99999) % 100
  if rand_mutation_rate > MutRate:
      return

  random_gene_index = np.random.randint(0, high = len(chromosome) - 1) 
  random_duration = [Day[np.random.randint(0, high=len(Day))]]

  # print(chromosome[random_gene_index][1][1]," ",str(random_duration[0]))
  # [3, 'Monday', '2pm-5pm', '', '1.2']
  chromosome[random_gene_index][1][1] = str(random_duration[0])

#Trả lại nhiễm sắc thể với thể trạng tốt nhất từ quần thể
def best_chromosome():
  fitness_array = []
  for i in range(len(Populations_array)):
      fitness_array.append(calculate_fitness(Populations_array[i]))
  maximum = max(fitness_array)
  max_index = fitness_array.index(maximum)

  return Populations_array[max_index]

def main():
    generation = 0
    initialize_population()
    asorter=[]
    while True:
        mybest_chromosome = best_chromosome()
        best_score = calculate_fitness(mybest_chromosome)
        print("Gen = ",generation," : Best Fitness = ", best_score, " Lenght = " ,len(Populations_array) )
        if (best_score >= 5):
            best_ch = mybest_chromosome
            # print("-----best-----")
            # print(best_ch)
            # print("-----")
            for key, value in best_ch.items():
                #print(value[1])
                asorter.append(value)
            a = sorted(asorter, key=lambda x: (x[1][0],x[1][1],x[1][2]))    
            # print(*a, sep = "\n")
            counters = 1
            for value in a:
              # if(len(value[2]) != 0):
                print(counters, " ",findNameCourseByName(course, value[0]) ,value[1], "count student" , len(value[2]))
                # print(counters, " ",value[0] ,value[1], value[2])
                counters += 1
            # for value in a:
            #   if(len(value[2]) == 0):
            #     print(counters, " ",value[0] ,value[1], "count student" , len(value[2]))
            #     counters += 1
            break

        n_childrens = 10

        for i in range(n_childrens // 2):
            returnedindexes = roulette_wheel_selection(Populations_array)
            # print("roulette_wheel_selection ",Populations_array[returnedindexes[0]])
            #print(Populations_array[returnedindexes[1]])
            c1, c2 = crossover(Populations_array[returnedindexes[0]], Populations_array[returnedindexes[1]])
            exchange_lowest_fitness(c1, c2)
        generation += 1


if __name__ == "__main__":
  main()
